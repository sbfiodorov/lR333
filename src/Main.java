import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Service service = new Service();
        ArrayList<Person> persons = new ArrayList<>();

        Phone phone = new Phone("8921845679", PhoneType.MOBILE);
        Email email = new Email("eagle@gmail.com");
        Address address = new Address("Boston", "greenStreet", "328", "3");
        Link link = new Link("Twitter", "@danaWhite");

        Person person = new Person("John", "Snow");
        Person person1 = new Person("Dana", "White");

        persons.add(person);
        persons.add(person1);

        service.addPhone(person, phone);
        service.addEmail(person, email);
        service.addAddress(person, address);
        service.addLink(person, link);
        System.out.println();
        service.getPersonContact();
        System.out.println();
//        service.removeContactForAllPersons(persons, Phone.class);
        System.out.println();
        service.searchPeopleBySubstringName(persons, "o");
        System.out.println();
        service.searchPeopleBySubstringSurName(persons, "e");
    }
}

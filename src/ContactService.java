import java.util.ArrayList;

public interface ContactService {
    void addPhone(Person person, Phone phone);

    void addEmail(Person person, Email email);

    void addAddress(Person person, Address address);

    void addLink(Person person, Link link);

    void removePhone(Person person);

    void removeEmail(Person person);

    void removeAddress(Person person);

    void removeLink(Person person);

    void removePerson(Person person);

    void removeContactForAllPersons(ArrayList<Person> people, Class<? extends Contact> contact);

    ArrayList<Person> getPersonContact();

    ArrayList<Person> getAllPerson();

    void searchPeopleBySubstringName(ArrayList<Person> personA, String substring);

    void searchPeopleBySubstringSurName(ArrayList<Person> personA, String substring);
}

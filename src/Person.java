import java.util.Objects;

public class Person {
    private String name;
    private String surName;
    private Phone phone;
    private Email email;
    private Address address;
    private Link link;

    Person(String name, String surName) {
        String regex = "[^a-zA-Z]";
        this.name = name.replaceAll(regex, "");
        this.surName = surName.replaceAll(regex, "");
    }

    String getName() {
        return name;
    }

    String getSurName() {
        return surName;
    }

    Phone getPhone() {
        return phone;
    }

    void setPhone(Phone phone) {
        this.phone = phone;
    }

    Email getEmail() {
        return email;
    }

    void setEmail(Email email) {
        this.email = email;
    }

    Address getAddress() {
        return address;
    }

    void setAddress(Address address) {
        this.address = address;
    }

    Link getLink() {
        return link;
    }

    void setLink(Link link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Person" +
                "{" +
                "name = " + name +
                ", surName = " + surName +
                ", phone = " + phone +
                ", email = " + email +
                ", address = " + address +
                ", link = " + link +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surName, person.surName) &&
                Objects.equals(phone, person.phone) &&
                Objects.equals(email, person.email) &&
                Objects.equals(address, person.address) &&
                Objects.equals(link, person.link);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surName, phone, email, address, link);
    }
}
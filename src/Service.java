import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Service implements ContactService {
    private ArrayList<Person> people = new ArrayList<>();
    private Map<Person, ArrayList<Phone>> map = new HashMap<>();

    @Override
    public void addPhone(Person person, Phone phone) {
        person.setPhone(phone);
    }

    @Override
    public void addEmail(Person person, Email email) {
        person.setEmail(email);
    }

    @Override
    public void addAddress(Person person, Address address) {
        person.setAddress(address);
    }

    @Override
    public void addLink(Person person, Link link) {
        person.setLink(link);
    }

    @Override
    public void removePhone(Person person) {
        if (person.getPhone() != null) {
            person.setPhone(null);
        }
    }

    @Override
    public void removeEmail(Person person) {
        if (person.getEmail() != null) {
            person.setEmail(null);
        }
    }

    @Override
    public void removeAddress(Person person) {
        if (person.getAddress() != null) {
            person.setAddress(null);
        }
    }

    @Override
    public void removeLink(Person person) {
        if (person.getLink() != null) {
            person.setLink(null);
        }
    }

    @Override
    public void removePerson(Person person) {
        people.remove(person);
    }

    @Override
    public void removeContactForAllPersons(ArrayList<Person> people, Class<? extends Contact> contact) {
        String contactType = contact.getName();
        System.out.println("contactType : " + contactType);

        for (Person p : people) {
            switch (contactType) {
                case "Phone":
                    System.out.println("removing phone");
                    p.setPhone(null);
                    break;
                case "Email":
                    System.out.println("removing email");
                    p.setEmail(null);
                    break;
                case "Address":
                    System.out.println("removing address");
                    p.setEmail(null);
                    break;
                case "Link":
                    System.out.println("removing link");
                    p.setEmail(null);
                    break;
            }
        }
    }

    @Override
    public ArrayList<Person> getPersonContact() {
        for (Person p : people) {
            if (p.getPhone() != null) {
                System.out.println(p);
            } else if (p.getEmail() != null) {
                System.out.println(p);
            } else if (p.getAddress() != null) {
                System.out.println(p);
            } else if (p.getLink() != null) {
                System.out.println(p);
            }
        }
        return people;
    }

    @Override
    public ArrayList<Person> getAllPerson() {
        return people;
    }


    @Override
    public void searchPeopleBySubstringName(ArrayList<Person> personA, String substring) {
        ArrayList<Person> people1 = new ArrayList<>();
        for (Person p : personA) {
            String str = p.getName();
            int index1 = str.indexOf(substring);

            if (index1 >= 0) {
                System.out.println("Найден");
                people1.add(p);
            }
        }
        people1.forEach(System.out::println);
    }

    @Override
    public void searchPeopleBySubstringSurName(ArrayList<Person> personA, String substring) {
        ArrayList<Person> people1 = new ArrayList<>();

        for (Person p : personA) {
            String str = p.getSurName();
            int index1 = str.indexOf(substring);

            if (index1 >= 0) {
                System.out.println("Найден");
                people1.add(p);
            }
        }
        people1.forEach(System.out::println);
    }
}
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;

class ServiceTest {
    private ArrayList<Person> people = new ArrayList<>();

    @org.junit.jupiter.api.Test
    void addPhone() {
        Person person = new Person("Dana", "White");
        Phone phone = new Phone("893434437456756", PhoneType.MOBILE);
        Assertions.assertEquals(person, person);
        person.setPhone(phone);
        Assertions.assertEquals(person, person);
    }

    @org.junit.jupiter.api.Test
    void addEmail() {
        Person person = new Person("Dana", "White");
        Email email = new Email("DanaWhite@gmail.com");
        Assertions.assertEquals(person, person);
        person.setEmail(email);
        Assertions.assertEquals(person, person);
    }

    @org.junit.jupiter.api.Test
    void addAddress() {
        Person person = new Person("Dana", "White");
        Address address = new Address("Boston", "greenStreet", "328", "3");
        Assertions.assertEquals(person, person);
        person.setAddress(address);
        Assertions.assertEquals(person, person);
    }

    @org.junit.jupiter.api.Test
    void addLink() {
        Person person = new Person("Dana", "White");
        Link link = new Link("Twitter", "@danaWhite");
        Assertions.assertEquals(person, person);
        person.setLink(link);
        Assertions.assertEquals(person, person);
    }

    @org.junit.jupiter.api.Test
    void removePhone() {
        String str = "Person{name = John, surName = Snow, phone = 894536357467}";
        Assertions.assertEquals(str, str);
        str = "Person{name = John, surName = Snow, phone = null}";
        Assertions.assertEquals(str, str);
    }

    @org.junit.jupiter.api.Test
    void removeEmail() {
        String str = "Person{name = John, surName = Snow, email = eagle@gmail.com}";
        Assertions.assertEquals(str, str);
        str = "Person{name = John, surName = Snow, email = null}";
        Assertions.assertEquals(str, str);
    }

    @org.junit.jupiter.api.Test
    void removeAddress() {
        String str = "Person{name = John, surName = Snow, address = city = Boston, street = greenStreet, numberHome = 328, apartmentNumber = 3}";
        Assertions.assertEquals(str, str);
        str = "Person{name = John, surName = Snow, address = null}";
        Assertions.assertEquals(str, str);
    }

    @org.junit.jupiter.api.Test
    void removeLink() {
        String str = "Person{name = John, surName = Snow, link = socialNetWork = Twitter, url = @danaWhite}";
        Assertions.assertEquals(str, str);
        str = "Person{name = John, surName = Snow, link = null}";
        Assertions.assertEquals(str, str);
    }

    @org.junit.jupiter.api.Test
    void removePerson() {
        Person person = new Person("John", "Snow");
        Person person1 = new Person("Dana", "White");
        Person person2 = new Person("Sean", "Shelby");
        people.add(person);
        people.add(person1);
        people.add(person2);
        Assertions.assertEquals(3, 3);
        people.remove(person2);
        Assertions.assertEquals(2, 2);
    }

    @org.junit.jupiter.api.Test
    void removeContactForAllPersons() {

    }

    @org.junit.jupiter.api.Test
    void viewAllPeopleContactData() {
        String str = "Person{name = John, surName = Snow, phone = Phone{number = 8921845679}, email = Email{email = eagle@gmail.com}, address = Address{city = Boston, street = greenStreet, numberHome = 328, apartmentNumber = 3}, link = Link{socialNetWork = Twitter, url = @danaWhite}}";
        Assertions.assertEquals(str, str);
    }

    @org.junit.jupiter.api.Test
    void searchPeopleBySubstringName() {

    }

    @org.junit.jupiter.api.Test
    void searchPeopleBySubstringSurName() {

    }
}
class Link extends Contact {
    private String socialNetWork;
    private String url;

    Link(String socialNetWork, String url) {
        String regex = "[^a-zA-Z]";
        String regex2 = "[^@a-zA-Z]";
        this.socialNetWork = socialNetWork.replaceAll(regex, "");
        this.url = url.replaceAll(regex2, "");
    }

    @Override
    public String toString() {
        return "Link" +
                "{" +
                "socialNetWork = " + socialNetWork +
                ", url = " + url +
                '}';
    }
}

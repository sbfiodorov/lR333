class Email extends Contact {
    private String email;

    Email(String email) {
        String regex2 = "[^a-zA-Z@.]";
        this.email = email.replaceAll(regex2, "");
    }

    @Override
    public String toString() {
        return "Email" +
                "{" +
                "email = " + email +
                "}";
    }
}

class Phone extends Contact {
    private String number;
    private PhoneType phoneType;

    Phone(String number, PhoneType phoneType) {
        String regex2 = "[^0-9]";
        this.number = number.replaceAll(regex2, "");
        this.phoneType = phoneType;
    }

    @Override
    public String toString() {
        return "Phone" +
                "{" +
                "number = " + number +
                ", phoneType = " + phoneType +
                '}';
    }
}

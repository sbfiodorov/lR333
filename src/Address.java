class Address extends Contact {
    private String city;
    private String street;
    private String numberHome;
    private String apartmentNumber;

    Address(String city, String street, String numberHome, String apartmentNumber) {
        String regex = "[^a-zA-Z]";
        String regex2 = "[^0-9]";
        this.city = city.replaceAll(regex, "");
        this.street = street.replaceAll(regex, "");
        this.numberHome = numberHome.replaceAll(regex2, "");
        this.apartmentNumber = apartmentNumber.replaceAll(regex2, "");
    }

    @Override
    public String toString() {
        return "Address" +
                "{" +
                "city = " + city +
                ", street = " + street +
                ", numberHome = " + numberHome +
                ", apartmentNumber = " + apartmentNumber +
                '}';
    }
}
